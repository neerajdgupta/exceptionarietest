package com.exceptionaire.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.exceptionaire.R;

public abstract class BaseDailogFragment extends DialogFragment  {

    // If require add common method , fields and logic


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.LoaderStyle);
    }
}

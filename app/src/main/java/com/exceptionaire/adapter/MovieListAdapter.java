package com.exceptionaire.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.exceptionaire.R;
import com.exceptionaire.databinding.LayoutMovieItemBinding;
import com.exceptionaire.model.MovieResponse;
import com.exceptionaire.view.fragment.TabOneFragment;

import java.util.ArrayList;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieViewHolder> {

    private ArrayList<MovieResponse> arrayList;

    private MovieListener movieListener;

    private Context context;

   public void setMovieList(ArrayList<MovieResponse> arrayList,TabOneFragment oneFragment){
       this.arrayList=arrayList;
       this.movieListener=oneFragment;
       this.context=oneFragment.getContext();
       notifyDataSetChanged();
   }


    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutMovieItemBinding itemBinding=  DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_movie_item,parent,false);
        return new MovieViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, final int position) {

       holder.itemBinding.tvTitle.setText(arrayList.get(position).getTitle());
       holder.itemBinding.tvYear.setText(String.valueOf(arrayList.get(position).getReleaseYear()));
        Glide.with(holder.itemBinding.imgMovie).load(arrayList.get(position).getImage()).into(holder.itemBinding.imgMovie);

        holder.itemBinding.btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movieListener.getMovieDetail(arrayList.get(position));
            }
        });

        holder.itemBinding.circleProgress.setProgress(Math.round(arrayList.get(position).getRating()));
        holder.itemBinding.circleProgress.setSecondaryProgress(10);
        holder.itemBinding.circleProgress.setProgressDrawable(context.getDrawable(R.drawable.custom_progressbar));
        holder.itemBinding.tvRating.setText(String.valueOf(arrayList.get(position).getRating()));

    }

    @Override
    public int getItemCount() {
        return arrayList==null?0:arrayList.size();
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {

        private LayoutMovieItemBinding itemBinding;

        MovieViewHolder(@NonNull LayoutMovieItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding=itemBinding;
        }
    }

   public interface MovieListener{
       void getMovieDetail(MovieResponse movieResponse);
    }
}

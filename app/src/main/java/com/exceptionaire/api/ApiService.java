package com.exceptionaire.api;

import com.exceptionaire.model.MovieResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiService {

    @GET("json/movies.json")
    Observable<List<MovieResponse>> getMovies();

}

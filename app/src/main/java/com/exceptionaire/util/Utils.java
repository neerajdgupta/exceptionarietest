package com.exceptionaire.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;


public class Utils {


    public static String handleError(Throwable throwable){
        String message;

        if(throwable instanceof HttpException){
                message = "HTTP REQUEST ERROR";
        }else if(throwable instanceof IOException) {
            message = "Please check internet connection";

        }else {
            message = "something went to wrong";

        }

        return message;

    }





    public static void showAlert(String message, Activity context) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

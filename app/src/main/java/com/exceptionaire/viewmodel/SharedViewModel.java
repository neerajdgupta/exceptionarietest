package com.exceptionaire.viewmodel;

import android.annotation.SuppressLint;

import androidx.lifecycle.MutableLiveData;

import com.exceptionaire.api.ApiService;
import com.exceptionaire.base.BaseViewModel;
import com.exceptionaire.model.MovieResponse;
import com.exceptionaire.network.ApiClient;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class SharedViewModel extends BaseViewModel {

    private MutableLiveData<List<MovieResponse>> moviesLiveData=new MutableLiveData<>();

    private MutableLiveData<MovieResponse> movieLiveData=new MutableLiveData<>();


    private MutableLiveData<Boolean> progress=new MutableLiveData<>();

    private MutableLiveData<Throwable> error=new MutableLiveData<>();

    public MutableLiveData<List<MovieResponse>> getMoviesLiveData() {
        return moviesLiveData;
    }

    public MutableLiveData<MovieResponse> getMovieLiveData() {
        return movieLiveData;
    }

    public MutableLiveData<Boolean> getProgress() {
        return progress;
    }

    public MutableLiveData<Throwable> getError() {
        return error;
    }

    @SuppressLint("CheckResult")
    public void getMovieList(){
       getMovieObservable().subscribeWith(getMovieObserver());
    }

    private Observable<List<MovieResponse>> getMovieObservable(){
      return   ApiClient.getClient().create(ApiService.class).getMovies().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableObserver<List<MovieResponse>> getMovieObserver(){
        return new DisposableObserver<List<MovieResponse>>() {
            @Override
            public void onNext(List<MovieResponse> movieResponses) {
                moviesLiveData.setValue(movieResponses);
            }

            @Override
            public void onError(Throwable e) {
                progress.setValue(false);
                error.setValue(e);
            }

            @Override
            public void onComplete() {
                progress.setValue(false);
            }
        };
    }




    @Override
    protected void onCleared() {
        super.onCleared();

        moviesLiveData=null;
        movieLiveData=null;
        progress=null;
        error=null;

    }
}

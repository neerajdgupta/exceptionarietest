package com.exceptionaire.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.exceptionaire.R;
import com.exceptionaire.base.BaseFragment;
import com.exceptionaire.databinding.TabTwoFragmentBinding;
import com.exceptionaire.model.MovieResponse;
import com.exceptionaire.viewmodel.SharedViewModel;

public class TabTwoFragment extends BaseFragment {

    private SharedViewModel viewModel;

    private TabTwoFragmentBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.tab_two_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null)
            viewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);


        viewModel.getMovieLiveData().observe(this, new Observer<MovieResponse>() {
            @Override
            public void onChanged(MovieResponse movieResponse) {
                if (movieResponse != null) {
                    setupView(movieResponse);
                    binding.svContent.setVisibility(View.VISIBLE);
                    binding.tvNoResult.setVisibility(View.GONE);

                }else {
                    binding.svContent.setVisibility(View.GONE);
                    binding.tvNoResult.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setupView(MovieResponse movieResponse) {


            binding.tvTitle.setText(movieResponse.getTitle());
            binding.tvYear.setText(String.valueOf(movieResponse.getReleaseYear()));
            Glide.with(binding.ivMovie).load(movieResponse.getImage()).into(binding.ivMovie);

            binding.circleProgress.setProgress(Math.round(movieResponse.getRating()));
            binding.circleProgress.setSecondaryProgress(10);
            binding.circleProgress.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progressbar));
            binding.tvRating.setText(String.valueOf(movieResponse.getRating()));


            if(movieResponse.getGenre()!=null && !(movieResponse.getGenre().isEmpty())){

                StringBuilder stringBuilder =new StringBuilder();

                for (String s:movieResponse.getGenre()){
                    stringBuilder.append("\n");
                    stringBuilder.append(s);
                }

                binding.tvGenre.setText(stringBuilder);
            }

    }

}

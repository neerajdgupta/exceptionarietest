package com.exceptionaire.view.fragment;


import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.exceptionaire.R;
import com.exceptionaire.base.BaseDailogFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoaderFragment extends BaseDailogFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_loader, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog d = getDialog();
        if (d!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            if (d.getWindow()!=null)
                d.getWindow().setLayout(width, height);
        }
    }

}

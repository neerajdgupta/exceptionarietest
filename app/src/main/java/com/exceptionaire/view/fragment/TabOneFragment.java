package com.exceptionaire.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.exceptionaire.R;
import com.exceptionaire.adapter.MovieListAdapter;
import com.exceptionaire.base.BaseFragment;
import com.exceptionaire.databinding.TabOneFragmentBinding;
import com.exceptionaire.model.MovieResponse;
import com.exceptionaire.util.Utils;
import com.exceptionaire.viewmodel.SharedViewModel;

import java.util.ArrayList;
import java.util.List;

public class TabOneFragment extends BaseFragment implements MovieListAdapter.MovieListener {


    private SharedViewModel viewModel;

    private TabOneFragmentBinding binding;

    private LoaderFragment loaderFragment;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding= DataBindingUtil.inflate(inflater,R.layout.tab_one_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loaderFragment = new LoaderFragment();

        if(getActivity()!=null)
        viewModel= ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        viewModel.getMovieList();
        viewModel.getProgress().setValue(true);

        viewModel.getMoviesLiveData().observe(this, new Observer<List<MovieResponse>>() {
            @Override
            public void onChanged(List<MovieResponse> movieResponses) {
                setupRecyclerView(movieResponses);

            }
        });

        viewModel.getProgress().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {

                if (aBoolean) {

                    if (getActivity() != null){
                        getActivity().getSupportFragmentManager().beginTransaction().
                                replace(android.R.id.content, loaderFragment, "LOADER_FRAGMENT").
                                commitAllowingStateLoss();}
                } else {

                    if (loaderFragment != null)
                        loaderFragment.dismiss();
                }
            }
        });

        viewModel.getError().observe(this, new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                Utils.showAlert(Utils.handleError(throwable),getActivity());
            }
        });

    }

    private void setupRecyclerView(List<MovieResponse> movieResponse) {

        if(movieResponse!=null){
        binding.rvMovieList.setHasFixedSize(true);
          RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        binding.rvMovieList.setLayoutManager(layoutManager);
         MovieListAdapter mAdapter = new MovieListAdapter();
        binding.rvMovieList.setAdapter(mAdapter);
        mAdapter.setMovieList((ArrayList<MovieResponse>) movieResponse,this);
        }
    }

    @Override
    public void getMovieDetail(MovieResponse movieResponse) {
        viewModel.getMovieLiveData().setValue(movieResponse);
    }
}

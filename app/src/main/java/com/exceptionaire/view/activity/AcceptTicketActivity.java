package com.exceptionaire.view.activity;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.exceptionaire.R;
import com.exceptionaire.adapter.PagerAdapter;
import com.exceptionaire.base.BaseActivity;
import com.exceptionaire.databinding.ActivityAcceptTicketBinding;
import com.exceptionaire.model.MovieResponse;
import com.exceptionaire.viewmodel.SharedViewModel;

public class AcceptTicketActivity extends BaseActivity   {

    private ActivityAcceptTicketBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         binding = DataBindingUtil.setContentView(this, R.layout.activity_accept_ticket);
        setViewModel();

        binding.viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        binding.tabLayout.setupWithViewPager(binding.viewPager);


    }

    private void setViewModel() {
        SharedViewModel sharedViewModel= ViewModelProviders.of(this).get(SharedViewModel.class);

        sharedViewModel.getMovieLiveData().observe(this, new Observer<MovieResponse>() {
            @Override
            public void onChanged(MovieResponse movieResponse) {
                binding.viewPager.setCurrentItem(1,true);
            }
        });

    }


}

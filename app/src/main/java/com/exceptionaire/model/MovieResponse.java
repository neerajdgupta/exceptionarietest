package com.exceptionaire.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieResponse {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("rating")
    @Expose
    private Float rating;
    @SerializedName("releaseYear")
    @Expose
    private Integer releaseYear;
    @SerializedName("genre")
    @Expose
    private List<String> genre = null;


    public String getTitle() {
        return title;
    }


    public String getImage() {
        return image;
    }


    public Float getRating() {
        return rating;
    }


    public Integer getReleaseYear() {
        return releaseYear;
    }


    public List<String> getGenre() {
        return genre;
    }


    @Override
    public String toString() {
        return "MovieResponse{" +
                "title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", rating=" + rating +
                ", releaseYear=" + releaseYear +
                ", genre=" + genre +
                '}';
    }
}
